#### For instructions in english go below.
---
# Conversor the archivos SPSS a CSV
  Herramienta para convertir archivos spss a csv.
  
## Prerequisitos
- Instalar Docker : https://www.youtube.com/watch?v=sAJVn2urNhE
- Clonar este projecto ejecutando el siguiente comando:

  ```git clone git@gitlab.com:yopero/spss.git```

## Uso

- Colocar los archivos SPSS en la carpeta `input` , ellos deben tener una extension `.sav`.
- Ejecutar el comando convertSpssToCsv.sh.
    
    ```./convertSpssToCsv.sh```
- Los archivos ya convertidos estaran en la carpeta `output` con el mismo nombre pero con extension `.csv`.

---

# SPSS to CSV converter
  Tool to convert spss files to csv.

## Prerequisites
- Install Docker on your computer: 

  https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04
- Clone this project in your computer by running the following command:

  ```git clone git@gitlab.com:yopero/spss.git```

## Usage

- Place your SPSS files in the `input` folder, they should have a .sav extension.
- Run the convertSpssToCsv.sh executable
    
    ```./convertSpssToCsv.sh```
- The converted files will be placed in the `output` folder with the same name but ending with `.csv` extension.